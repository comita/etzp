package ru.at.etzp.tests.docflow;

import com.codeborne.selenide.Selenide;
import org.testng.annotations.Test;
import ru.at.etzp.helpers.senders.AuctionNoticeSender;
import ru.at.etzp.models.documents.notice.ClosedAuctionNotice;
import ru.at.etzp.models.documents.notice.OpenAuctionNotice;
import ru.at.etzp.models.documents.notice.RealEstateAuctionNotice;
import ru.at.etzp.models.documents.notice.RealEstateDownAuctionNotice;
import ru.at.etzp.models.users.OrganizerUser;
import ru.at.etzp.pages.dashboard.LoginPage;
import ru.at.etzp.pages.dashboard.TopMenuPage;
import ru.at.etzp.tests.BaseTest;
import ru.at.etzp.utils.Log;

public class AuctionFewTimes extends BaseTest {
    public enum AuctionType {OPEN_AUCTION, REAL_ESTATE, REAL_ESTATE_DOWN, CLOSED_AUCTION}

    @Test
    public void createAndSaveOpenAuctionNoticeSeveralTimes() {
        createAndSaveAuctionNoticeSeveralTimes(AuctionType.OPEN_AUCTION);
    }

    @Test
    public void createAndSendOpenAuctionNoticeSeveralTimes() {
        createAndSendNoticeSeveralTimes(AuctionType.OPEN_AUCTION);
    }

    @Test
    public void createAndSaveClosedAuctionNoticeSeveralTimes() {
        createAndSaveAuctionNoticeSeveralTimes(AuctionType.CLOSED_AUCTION);
    }

    @Test
    public void createAndSendClosedAuctionNoticeSeveralTimes() {
        createAndSendNoticeSeveralTimes(AuctionType.CLOSED_AUCTION);
    }

    @Test
    public void createAndSaveRealEstateAuctionNoticeSeveralTimes() {
        createAndSaveAuctionNoticeSeveralTimes(AuctionType.REAL_ESTATE);
    }

    @Test
    public void createAndSendRealEstateAuctionNoticeSeveralTimes() {
        createAndSendNoticeSeveralTimes(AuctionType.REAL_ESTATE);
    }

    @Test
    public void createAndSaveRealEstateDownAuctionNoticeSeveralTimes() {
        createAndSaveAuctionNoticeSeveralTimes(AuctionType.REAL_ESTATE_DOWN);
    }

    @Test
    public void createAndSendRealEstateDownAuctionNoticeSeveralTimes() {
        createAndSendNoticeSeveralTimes(AuctionType.REAL_ESTATE_DOWN);
    }

    private void createAndSaveAuctionNoticeSeveralTimes(AuctionType auctionType) {
        LoginPage.loginAndConfirmAs(new OrganizerUser());

        for (int i = 0; i < 50; i++) {
            AuctionNoticeSender.createAndSaveAuctionNotice(getNotice(auctionType));
            Log.write("Создано извещение № " + i);
            Selenide.sleep(4000);
        }

        TopMenuPage.logout();
    }

    private void createAndSendNoticeSeveralTimes(AuctionType auctionType) {
        LoginPage.loginAndConfirmAs(new OrganizerUser());

        for (int i = 0; i < 50; i++) {
            AuctionNoticeSender.createAndSendAuctionNotice(getNotice(auctionType));
            Log.write("Отправлено извещение № " + i);
            Selenide.sleep(4000);
        }

        TopMenuPage.logout();
    }

    private OpenAuctionNotice getNotice(AuctionType auctionType) {
        switch (auctionType) {
            case REAL_ESTATE:
                return new RealEstateAuctionNotice();
            case REAL_ESTATE_DOWN:
                return new RealEstateDownAuctionNotice();
            case CLOSED_AUCTION:
                return new ClosedAuctionNotice();
            default:
                return new OpenAuctionNotice();
        }
    }
}
