package ru.at.etzp.tests.docflow;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import ru.at.etzp.helpers.senders.OpenCompetitionNoticeSender;
import ru.at.etzp.models.documents.Protocol;
import ru.at.etzp.models.documents.RebiddingOffline;
import ru.at.etzp.models.documents.Request;
import ru.at.etzp.models.documents.notice.OpenCompetitionNotice;
import ru.at.etzp.models.users.OrganizerUser;
import ru.at.etzp.models.users.ParticipantUser;
import ru.at.etzp.pages.dashboard.*;
import ru.at.etzp.pages.editor.NoticePage;
import ru.at.etzp.pages.editor.ProtocolPage;
import ru.at.etzp.pages.editor.SendResultPage;
import ru.at.etzp.pages.editor.TopEditorPage;
import ru.at.etzp.pages.site.ProcedureDocumentationPage;
import ru.at.etzp.pages.site.ProcedureLotsPage;
import ru.at.etzp.pages.site.ProceduresListPage;
import ru.at.etzp.pages.site.SitePage;
import ru.at.etzp.tests.BaseTest;
import ru.at.etzp.utils.Files;

public class OpenCompetitionDocflowTest extends BaseTest {

    private OrganizerUser organizerUser;
    private ParticipantUser participantUser;
    private OpenCompetitionNotice openCompetitionNotice;
    private Request request;
    private Protocol protocol;
    private RebiddingOffline rebiddingOffline;

    @BeforeClass
    public void initialSteps() {
        organizerUser = new OrganizerUser();
        participantUser = new ParticipantUser();
        openCompetitionNotice = new OpenCompetitionNotice();
        request = new Request(openCompetitionNotice);
        protocol = new Protocol(openCompetitionNotice);
        rebiddingOffline = new RebiddingOffline(openCompetitionNotice);
    }

    @Test(priority = 1)
    public void sendOpenCompetitionNotice() {
        OpenCompetitionNoticeSender.sendOpenCompetitionNotice(organizerUser, openCompetitionNotice);
    }

    @Test(priority = 2, dependsOnMethods = {"sendOpenCompetitionNotice"})
    public void searchProcedureByNumber() {
        SitePage.openSitePage();
        SitePage.goToSearch();
        ProceduresListPage.searchByNumber(openCompetitionNotice.getNumber());
    }

    @Test(priority = 3, dependsOnMethods = {"searchProcedureByNumber"})
    public void openProcedurePage() {
        ProceduresListPage.openSearchResult();
        ProcedureDocumentationPage.checkPageLoadedCorrectly();
    }

    @Test(priority = 4, dependsOnMethods = {"openProcedurePage"})
    public void sendRequest() {
        ProcedureDocumentationPage.clickRequestButton();
        LoginPage.loginAndConfirmAs(participantUser);
        TopEditorPage.saveAndSend();
        AddFilesPage.clickContinue();
        SendResultPage.checkResultIsCorrect();
        SendResultPage.close();
        TopMenuPage.logout();
    }

    @Test(priority = 5, dependsOnMethods = {"sendRequest"})
    public void receiveRequest() {
        LoginPage.loginAndConfirmAs(organizerUser);
        AccordionMenuPage.receiveRequests();
        DialogPage.close();
        AccordionMenuPage.goToRootFolder();
        RootFolderPage.searchForDocument(request);
        TopMenuPage.logout();
    }

    @Test(priority = 6, dependsOnMethods = {"receiveRequest"})
    public void sendOffer() {
        SitePage.openSitePage();
        SitePage.goToSearch();
        ProceduresListPage.searchByNumber(openCompetitionNotice.getNumber());
        ProceduresListPage.openSearchResult();
        ProcedureDocumentationPage.goToLotsTab();
        ProcedureLotsPage.makeOfferForFirstLot();
        LoginPage.loginAndConfirmAs(participantUser);
        TopEditorPage.saveAndSend();
        AddFilesPage.addFile(Files.getPathToFile(Files.OPEN_PART_FILENAME));
        AddFilesPage.addFile(Files.getPathToFile(Files.CLOSED_PART_FILENAME));
        AddFilesPage.clickContinue();
        SendResultPage.checkResultIsCorrect();
        SendResultPage.close();
        TopMenuPage.logout();
    }

    @Test(priority = 7, dependsOnMethods = {"sendOffer"})
    public void makeProtocol() {
        openCompetitionNotice.setEndDateToCurrent();
        LoginPage.loginAndConfirmAs(organizerUser);
        AccordionMenuPage.goToRootFolder();
        RootFolderPage.searchForDocument(openCompetitionNotice);
        RootFolderPage.openContextMenu();
        ContextMenuPage.changeNotice();
        NoticePage.setEndOfferDate(openCompetitionNotice);
        TopEditorPage.saveAndSend();
        AddFilesPage.clickContinue();
        SendResultPage.checkResultIsCorrect();
        SendResultPage.close();
        RootFolderPage.searchForDocument(openCompetitionNotice);
        RootFolderPage.makeProtocol();
        MakingProtocolPage.checkProtocolIsDone();
        MakingProtocolPage.close();
    }

    @Test(priority = 8, dependsOnMethods = {"makeProtocol"})
    public void makeInvitesForRebiddingOffline() {
        RootFolderPage.searchForDocument(protocol);
        RootFolderPage.openContextMenu();
        ContextMenuPage.editDocument();
        ProtocolPage.admitAllParticipants();
        ProtocolPage.addRebiddingOffline(rebiddingOffline);
        ProtocolPage.makeInvites();
        TopEditorPage.close();
        DialogPage.confirm();
    }

    @Test(priority =  9, dependsOnMethods = {"makeInvitesForRebiddingOffline"})
    public void sendInvitesForRebiddingOffline() {
        AccordionMenuPage.goToDraftFolder();
        StandartFolderPage.searchForDocument(rebiddingOffline);
        StandartFolderPage.selectAllDocuments();
        ActionsMenuPage.signAndSend();
        SendResultPage.checkResultIsCorrect();
        SendResultPage.close();
        TopMenuPage.logout();
    }

}