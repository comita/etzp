package ru.at.etzp.tests.participant;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;
import ru.at.etzp.models.users.BaseUser;
import ru.at.etzp.models.users.ParticipantUser;
import ru.at.etzp.pages.dashboard.LoginPage;
import ru.at.etzp.pages.dashboard.TopMenuPage;
import ru.at.etzp.tests.BaseTest;

public class ParticipantLoginTest extends BaseTest {

    protected BaseUser createUser() {
        return new ParticipantUser();
    }

    @Test
    public void testLoginWithEmptyLoginAndPassword() {
        BaseUser user = createUser();
        user.setEmptyLogin().setEmptyPassword();

        LoginPage.loginAs(user);

        Assert.assertTrue(LoginPage.errorMessageIsVisible(), "Не появилась ошибка при попытке входа в ЛК с пустыми логином и паролем.");
    }

    @Test
    public void testLoginWithIncorrectLogin() {
        BaseUser user = createUser();
        user.setIncorrectLogin();

        LoginPage.loginAs(user);

        Assert.assertTrue(LoginPage.errorMessageIsVisible(), "Не появилась ошибка при попытке входа в ЛК с некорректным логином.");
    }

    @Test
    public void testLoginWithIncorrectPassword() {
        BaseUser user = createUser();
        user.setIncorrectPassword();

        LoginPage.loginAs(user);

        Assert.assertTrue(LoginPage.errorMessageIsVisible(), "Не появилась ошибка при попытке входа в ЛК с некорректным паролем.");
    }

    @Test
    public void testLoginWithCorrectLoginAndPassword() {
        BaseUser user = createUser();

        LoginPage.loginAndConfirmAs(user);

        TopMenuPage.checkPageLoadedCorrectly();
    }

    @AfterMethod (alwaysRun = true)
    public void logout() {
        if (TopMenuPage.keyLabelIsVisible()) TopMenuPage.logout();
    }

}