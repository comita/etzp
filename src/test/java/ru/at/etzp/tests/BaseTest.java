package ru.at.etzp.tests;

import com.codeborne.selenide.testng.TextReport;
import org.testng.SkipException;
import org.testng.annotations.*;
import ru.at.etzp.Core;
import ru.at.etzp.utils.WebDriverFactory;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

@Listeners({ TextReport.class})
public abstract class BaseTest {

    protected static Core core;

    @BeforeSuite
    public void init() {
        core = new Core();
    }

    @Parameters({"IEOnly"})
    @BeforeClass
    public void checkBrowserParameter(@Optional("IEOnly") boolean IEOnly) {
        if (IEOnly && !core.getBrowserProperties().getName().equals(WebDriverFactory.INTERNET_EXPLORER)) {
            throw new SkipException("Тест пропущен, т.к. браузер не Internet Explorer.");
        }
    }

    @AfterSuite(alwaysRun = true)
    public void tearDown() {
        getWebDriver().quit();
    }

}