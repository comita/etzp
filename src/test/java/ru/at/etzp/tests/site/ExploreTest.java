package ru.at.etzp.tests.site;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.at.etzp.pages.site.ProceduresListPage;
import ru.at.etzp.pages.site.SitePage;
import ru.at.etzp.tests.BaseTest;

public class ExploreTest extends BaseTest {

    @BeforeMethod
    public void openSitePage() {
        SitePage.openSitePage();
    }

    @AfterMethod
    public void checkPage() {
        ProceduresListPage.checkPageLoadedCorrectly();
    }

    @Test
    public void openCompetitionPage() {
        SitePage.goToOpenCompetition();
    }

    @Test
    public void twoStageCompetitionPage() {
        SitePage.goToTwoStageCompetition();
    }

    @Test
    public void openAuctionPage() {
        SitePage.goToOpenAuction();
    }

    @Test
    public void openRealEstateAuctionPage() {
        SitePage.goToRealEstateAuction();
    }

    @Test
    public void openRequestQuotationsPage() {
        SitePage.goToRequestQuotations();
    }

    @Test
    public void openRequestOffersPage() {
        SitePage.goToRequestOffers();
    }

    @Test
    public void openPrequalifyPage() {
        SitePage.goToPrequalify();
    }

    @Test
    public void openLimitedCompetitionPage() {
        SitePage.goToLimitedCompetition();
    }

    @Test
    public void openLimitedAuctionPage() {
        SitePage.goToLimitedAuction();
    }

    @Test
    public void openArchivePage() {
        SitePage.goToArchive();
    }

}