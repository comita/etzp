package ru.at.etzp.tests.organizer;

import ru.at.etzp.models.users.BaseUser;
import ru.at.etzp.models.users.OrganizerUser;
import ru.at.etzp.tests.participant.ParticipantLoginTest;

public class OrganizerLoginTest extends ParticipantLoginTest {

    @Override
    protected BaseUser createUser() {
        return new OrganizerUser();
    }
}