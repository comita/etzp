package ru.at.etzp;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.WebDriverRunner;
import org.openqa.selenium.WebDriver;
import ru.at.etzp.utils.Browser;
import ru.at.etzp.utils.PropertyLoader;
import ru.at.etzp.utils.WebDriverFactory;

import java.io.File;
import java.util.concurrent.TimeUnit;

public class Core {

    private Browser browserProperties;

    public static final int WAIT_TIMEOUT_SECONDS = 40000;

    public Core() {
        System.setProperty("java.util.logging.SimpleFormatter.format", "%1$tT %4$s %5$s%6$s%n");

        String gridHubUrl = PropertyLoader.loadProperty(PropertyLoader.GRID_HUB);

        setBrowserProperties();

        WebDriver webDriver = WebDriverFactory.createWebDriver(browserProperties, gridHubUrl);
        webDriver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
        webDriver.manage().window().maximize();

        WebDriverRunner.setWebDriver(webDriver);

        Configuration.timeout = WAIT_TIMEOUT_SECONDS;
        Configuration.reportsFolder = System.getProperty("user.dir") + File.separator + "test-output" + File.separator + "screenshots";
    }

    private void setBrowserProperties() {
        browserProperties = new Browser().setName(PropertyLoader.loadProperty(PropertyLoader.BROWSER_NAME));
    }

    public Browser getBrowserProperties() {
        return browserProperties;
    }

}