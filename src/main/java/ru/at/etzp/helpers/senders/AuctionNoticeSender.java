package ru.at.etzp.helpers.senders;

import com.thoughtworks.selenium.condition.Not;
import ru.at.etzp.models.documents.notice.ClosedAuctionNotice;
import ru.at.etzp.models.documents.notice.OpenAuctionNotice;
import ru.at.etzp.models.documents.notice.RealEstateAuctionNotice;
import ru.at.etzp.models.documents.notice.RealEstateDownAuctionNotice;
import ru.at.etzp.pages.dashboard.*;
import ru.at.etzp.pages.editor.NoticePage;
import ru.at.etzp.pages.editor.SendResultPage;
import ru.at.etzp.pages.editor.TopEditorPage;

public class AuctionNoticeSender {
    public static void createAndSaveAuctionNotice(OpenAuctionNotice notice) {
        AccordionMenuPage.createNotice();

        fillNotice(notice);

        TopEditorPage.saveAndClose();
        DialogPage.confirm();
    }

    private static void fillNotice(OpenAuctionNotice notice) {
        if (notice instanceof RealEstateDownAuctionNotice) {
            NoticePage.fillAsRealEstateDownAuction(((RealEstateDownAuctionNotice) notice));
            return;
        }

        if (notice instanceof RealEstateAuctionNotice) {
            NoticePage.fillAsRealEstateAuction(((RealEstateAuctionNotice) notice));
            return;
        }

        if (notice instanceof ClosedAuctionNotice) {
            NoticePage.fillAsClosedAuction(((ClosedAuctionNotice) notice));
            return;
        }

        NoticePage.fillAsOpenAuction(notice);
    }

    public static void createAndSendAuctionNotice(OpenAuctionNotice notice) {
        AccordionMenuPage.createNotice();

        fillNotice(notice);

        TopEditorPage.saveAndSend();
        AddFilesPage.clickContinue();

        if (notice instanceof  ClosedAuctionNotice)
            AddRecipientsPage.clickContinue();

        SendResultPage.checkResultIsCorrect();
        SendResultPage.close();
    }
}
