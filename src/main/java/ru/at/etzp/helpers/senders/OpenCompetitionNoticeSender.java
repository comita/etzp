package ru.at.etzp.helpers.senders;

import ru.at.etzp.models.documents.notice.OpenCompetitionNotice;
import ru.at.etzp.models.users.OrganizerUser;
import ru.at.etzp.pages.dashboard.AccordionMenuPage;
import ru.at.etzp.pages.dashboard.AddFilesPage;
import ru.at.etzp.pages.dashboard.LoginPage;
import ru.at.etzp.pages.dashboard.TopMenuPage;
import ru.at.etzp.pages.editor.NoticePage;
import ru.at.etzp.pages.editor.SendResultPage;
import ru.at.etzp.pages.editor.TopEditorPage;

public class OpenCompetitionNoticeSender {

    public static void sendOpenCompetitionNotice(OrganizerUser organizerUser, OpenCompetitionNotice openCompetitionNotice) {
        LoginPage.loginAndConfirmAs(organizerUser);
        AccordionMenuPage.createNotice();
//        NoticePage.importNomenclatureFile(Files.getPathToFile(Files.NOMENCLATURE_FILENAME));
        NoticePage.fillAs(openCompetitionNotice);
        TopEditorPage.saveAndSend();
//        AddFilesPage.addFile(Files.getPathToFile(Files.DOCUMENTATION_ONE_FILENAME));
        AddFilesPage.clickContinue();
        SendResultPage.checkResultIsCorrect();
        SendResultPage.close();
        TopMenuPage.logout();
    }

}
