package ru.at.etzp.utils;

public class Browser {

    private String name;

    public String getName() {
        return name;
    }

    public Browser setName(String name) {
        this.name = name;
        return this;
    }

}