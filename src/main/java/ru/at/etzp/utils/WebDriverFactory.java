package ru.at.etzp.utils;

import org.openqa.selenium.Proxy;
import org.openqa.selenium.UnexpectedAlertBehaviour;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.LocalFileDetector;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

public class WebDriverFactory {
    public static final String CHROME = "chrome";
    public static final String FIREFOX = "firefox";
    public static final String INTERNET_EXPLORER = "ie";

    public static WebDriver createWebDriver(Browser browser, String gridHubUrl) {
        DesiredCapabilities capabilities;

        switch (browser.getName()) {
            case CHROME:
                capabilities = DesiredCapabilities.chrome();
                break;

            case FIREFOX:
                capabilities = DesiredCapabilities.firefox();
                break;

            default:
                Log.write("Ошибка: указано некорректное название браузера. Будет запущен браузер по умолчанию: Internet Explorer.");

            case INTERNET_EXPLORER:
                capabilities = DesiredCapabilities.internetExplorer();
//                capabilities.setCapability(InternetExplorerDriver.REQUIRE_WINDOW_FOCUS, true);
                capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.ACCEPT);
                capabilities.setCapability(InternetExplorerDriver.LOG_LEVEL, "TRACE");

                Proxy proxy = new Proxy();
                proxy.setProxyType(Proxy.ProxyType.DIRECT);
                capabilities.setCapability(CapabilityType.PROXY, proxy);

                // TODO сохранять в test-output, вынести название папки в application.properties
                capabilities.setCapability(InternetExplorerDriver.LOG_FILE, "test-output" + File.separator + INTERNET_EXPLORER + ".log");
                break;
        }

        // Если нет адреса на удаленную машину, то запускаем локально
        if (gridHubUrl == null || gridHubUrl.length() == 0) {
            return startLocalBrowser(browser, capabilities);
        }

        // Запустить удаленно
        return startRemoteBrowser(capabilities, gridHubUrl);
    }

    private static WebDriver startLocalBrowser(Browser browser, DesiredCapabilities capabilities) {
        WebDriver localWebDriver;

        switch (browser.getName()) {
            case CHROME:
                localWebDriver = new ChromeDriver(capabilities);
                break;

            case FIREFOX:
                localWebDriver = new FirefoxDriver(capabilities);
                break;

            default:
            case INTERNET_EXPLORER:
                localWebDriver = new InternetExplorerDriver(capabilities);
                break;
        }

        return localWebDriver;
    }

    private static WebDriver startRemoteBrowser(DesiredCapabilities capabilities, String gridHubUrl) {
        RemoteWebDriver remoteWebDriver = null;

        try {
            remoteWebDriver = new RemoteWebDriver(new URL(gridHubUrl), capabilities);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        remoteWebDriver.setFileDetector(new LocalFileDetector());

        return remoteWebDriver;
    }

}