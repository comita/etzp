package ru.at.etzp.utils;

import java.io.File;
import java.net.URISyntaxException;
import java.nio.file.Paths;

public class Resources {

    public static String getAbsolutePathForFileResource(String fileName) {
        fileName = "files" + File.separator + fileName;
        String absolutePath = "no path";

        try {
            absolutePath = Paths.get(ClassLoader.getSystemResource(fileName).toURI()).toString();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        return absolutePath;
    }
}