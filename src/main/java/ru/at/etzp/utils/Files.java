package ru.at.etzp.utils;

public class Files {

    public static final String DOCUMENTATION_ONE_FILENAME = "документация1.txt";
    public static final String DOCUMENTATION_TWO_FILENAME = "документация2.txt";
    public static final String DOCUMENTATION_THREE_FILENAME = "документация3.txt";

    public static final String NOMENCLATURE_FILENAME = "номенклатура.xls";

    public static final String OPEN_PART_FILENAME = "ОЧ.txt";
    public static final String CLOSED_PART_FILENAME = "ЗЧ.txt";

    public static final String EXPLANATIONS_ONE_FILENAME = "разъяснения1.xls";
    public static final String EXPLANATIONS_TWO_FILENAME = "разъяснения2.xls";
    public static final String EXPLANATIONS_THREE_FILENAME = "разъяснения3.xls";

    public static final String NOTIFICATION_ONE_FILENAME = "уведомление1.xls";
    public static final String NOTIFICATION_TWO_FILENAME = "уведомление2.xls";

    public static final String PROTOCOL_ONE_FILENAME = "протокол1.xls";
    public static final String PROTOCOL_TWO_FILENAME = "протокол2.xls";

    public static final String CANCEL_FILENAME = "отказ.xls";

    public static String getPathToFile(String filename) {
        return Resources.getAbsolutePathForFileResource(filename);
    }

}
