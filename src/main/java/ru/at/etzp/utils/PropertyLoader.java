package ru.at.etzp.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyLoader {

    public static final String GRID_HUB = "grid2.hub";

    public static final String BROWSER_NAME = "browser.name";
    public static final String BROWSER_VERSION = "browser.version";
    public static final String BROWSER_PLATFORM = "browser.platform";

    public static final String URL_SITE = "url.site";
    public static final String URL_ORGANIZER = "url.organizer";
    public static final String URL_PARTICIPANT = "url.participant";

    private static final String PROPERTIES_FILE = "/application.properties";

    public static String loadProperty(String name) {
        Properties properties = new Properties();

        try {
            properties.load(PropertyLoader.class.getResourceAsStream(PROPERTIES_FILE));
        } catch (IOException e) {
            e.printStackTrace();
        }

        String value = "";

        if (name != null) {
            value = properties.getProperty(name);
            if (value != null && !value.isEmpty() && value.charAt(0) == '$') {
                value = "";
            }
        }

        return value;
    }

}