package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Selenide.$;

public class AddFilesPage extends BasePage {

    private static final By FILE_INPUT = id("fileItem");
    private static final By ADD_FILE_BUTTON = id("addAttachmentButton");
    private static final By UPLOAD_PROGRESS = id("uploadProgressContainer");
    private static final By CONTINUE_BUTTON = id("signOnLoadAttachmentsContinueBtn");

    public static void addFile(String filePath) {
        $(FILE_INPUT).setValue(filePath);
        $(ADD_FILE_BUTTON).click();
        $(UPLOAD_PROGRESS).should(disappear);
    }

    public static void clickContinue() {
        $(CONTINUE_BUTTON).click();
    }
}
