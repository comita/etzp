package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class ActionsMenuPage extends BasePage {

    private static final By ACTIONS_LIST = id("menu");

    private final static String MAKE_PROTOCOL_VALUE = "makeProtocols";
    private final static String SIGN_AND_SEND_VALUE = "SIGN_AND_SEND";

    public static void makeProtocol() {
        $(ACTIONS_LIST).selectOptionByValue(MAKE_PROTOCOL_VALUE);
    }

    public static void signAndSend() {
        $(ACTIONS_LIST).selectOptionByValue(SIGN_AND_SEND_VALUE);
    }

}
