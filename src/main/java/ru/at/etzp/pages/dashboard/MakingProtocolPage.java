package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class MakingProtocolPage extends BasePage {

    private static final By PROGRESS_TEXT_AREA = id("eT");
    private static final By CLOSE_BUTTON = css("div[role=dialog] button");

    private static final String MAKING_PROTOCOL_IS_FINISHED = "Построение протоколов завершено";

    public static void checkProtocolIsDone() {
        $(PROGRESS_TEXT_AREA).shouldHave(text(MAKING_PROTOCOL_IS_FINISHED));
    }

    public static void close() {
        $(CLOSE_BUTTON).click();
    }

}
