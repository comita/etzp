package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class AddRecipientsPage extends BasePage {

    private static final By CONTINUE_BUTTON = id("signOnLoadRecipientsContinueBtn");

    public static void clickContinue() {
        $(CONTINUE_BUTTON).click();
    }

}
