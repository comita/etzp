package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class DialogPage extends BasePage {

    private static final By CONFIRM_BUTTON = css("div[role=dialog] button:first-child");
    private static final By CANCEL_BUTTON = css("div[role=dialog] button:first-child + button");
    private static final By CLOSE_BUTTON = css(".ui-icon.ui-icon-closethick");

    public static void confirm() {
        $(CONFIRM_BUTTON).click();
    }

    public static void cancel() {
        $(CANCEL_BUTTON).click();
    }

    public static void close() {
        $(CLOSE_BUTTON).click();
    }

}
