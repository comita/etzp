package ru.at.etzp.pages.dashboard;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.attribute;
import static com.codeborne.selenide.Selenide.$;

public class AccordionMenuPage extends BasePage {

    private static final By NAVIGATOR_SECTION = css("#navAccordion div:first-child h3");
    private static final By ROOT_FOLDER_BUTTON= css("#navAccordion .docFolder[onclick*='root']");
    private static final By DRAFT_FOLDER_BUTTON = css("#navAccordion .docFolder[onclick*='draft']");

    private static final By CREATE_DOCUMENT_SECTION = css("#navAccordion div:first-child+div h3");
    private static final By CREATE_NOTICE_BUTTON = css("#navAccordion .selectableElement[onclick*='NOTICEZK']");

    private static final By RECEIVE_DOCUMENTS_SECTION = css("#navAccordion div:first-child+div+div h3");
    private static final By RECEIVE_REQUESTS_BUTTON= css("#navAccordion .selectableElement[onclick*='INQUIRYORG']");

    private static final String AREA_EXPANDED_ATTRIBUTE = "aria-expanded";

    public static void goToRootFolder() {
        $(NAVIGATOR_SECTION).click();
        $(NAVIGATOR_SECTION).shouldHave(attribute(AREA_EXPANDED_ATTRIBUTE, "true"));
        $(ROOT_FOLDER_BUTTON).click();
    }

    public static void goToDraftFolder() {
        $(NAVIGATOR_SECTION).click();
        $(NAVIGATOR_SECTION).shouldHave(attribute(AREA_EXPANDED_ATTRIBUTE, "true"));
        $(DRAFT_FOLDER_BUTTON).click();
    }

    public static void createNotice() {
        $(CREATE_DOCUMENT_SECTION).click();
        Selenide.sleep(2500);
        $(CREATE_NOTICE_BUTTON).click();
    }

    public static void receiveRequests() {
        $(RECEIVE_DOCUMENTS_SECTION).click();
        $(RECEIVE_DOCUMENTS_SECTION).shouldHave(attribute(AREA_EXPANDED_ATTRIBUTE, "true"));
        $(RECEIVE_REQUESTS_BUTTON).click();
    }

}
