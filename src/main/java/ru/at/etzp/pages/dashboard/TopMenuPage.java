package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class TopMenuPage extends BasePage {

    public static final String LOGOUT_BUTTON_ID = "logOutButton";
    private static final By LOGOUT_BUTTON = id(LOGOUT_BUTTON_ID);
    private static final By KEY_lABEL= id("keySubject");


    public static void logout() {
        $(LOGOUT_BUTTON).click();
        DialogPage.confirm();
        LoginPage.waitForPageToLoad();
    }

    public static void checkPageLoadedCorrectly() {
        $(KEY_lABEL).shouldBe(visible);
    }

    public static boolean keyLabelIsVisible() {
        return $(KEY_lABEL).is(visible);
    }

}
