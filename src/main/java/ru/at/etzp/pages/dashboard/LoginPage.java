package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.tools.ElementDetector;
import ru.at.etzp.models.users.BaseUser;
import ru.at.etzp.pages.BasePage;
import ru.at.etzp.utils.PropertyLoader;

import static com.codeborne.selenide.Condition.exist;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class LoginPage extends BasePage {
    private static final By LOGIN_FIELD = id("login");
    private static final By PASSWORD_FIELD = id("password");
    private static final String LOGIN_BUTTON_ID = "loginBtn";
    private static final By LOGIN_BUTTON = id(LOGIN_BUTTON_ID);

    private static final By ERROR_MESSAGE = css(".ui-state-error");

    public static void loginAs(BaseUser user) {
        openLoginPage(user);
        $(LOGIN_FIELD).click();
        $(LOGIN_FIELD).val(user.getLogin());
        $(PASSWORD_FIELD).val(user.getPassword());
        $(LOGIN_BUTTON).click();
    }

    private static void openLoginPage(BaseUser user) {
        if (user.getRole() == BaseUser.Role.Participant) {
            open(PropertyLoader.loadProperty(PropertyLoader.URL_PARTICIPANT));
        } else {
            open(PropertyLoader.loadProperty(PropertyLoader.URL_ORGANIZER));
        }
    }

    public static void loginAndConfirmAs(BaseUser user) {
        loginAs(user);
        String elementId = ElementDetector.idThatPresentsOnPage(LOGIN_BUTTON_ID, TopMenuPage.LOGOUT_BUTTON_ID);
        if (elementId.equals(LOGIN_BUTTON_ID)) $(LOGIN_BUTTON).click();
    }

    public static boolean errorMessageIsVisible() {
        return $(ERROR_MESSAGE).is(visible);
    }

    public static void waitForPageToLoad() {
        $(LOGIN_BUTTON).should(exist);
    }
}
