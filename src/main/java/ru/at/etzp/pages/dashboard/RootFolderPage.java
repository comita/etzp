package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.models.documents.BaseDocument;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class RootFolderPage extends BasePage {

    private static final By DOCUMENT_TYPE_LIST = id("filterDocumentType");
    private static final By PROCEDURE_NUMBER_INPUT = id("filterContestNumber");
    private static final By SEARCH_BUTTON = id("rootFindButton");
    private static final By CLEAR_BUTTON = id("rootClearButton");

    private static final By RESULT_PROCEDURE_CHECKBOX = css("#doclistCenter tr:first-child+tr [name=idlist]");
    private static final By RESULT_PROCEDURE_NUMBER = css("#doclistCenter tr:first-child+tr td:first-child+td+td+td+td span:first-child");

    public static void searchForDocument(BaseDocument document) {
        $(CLEAR_BUTTON).click();
        $(DOCUMENT_TYPE_LIST).selectOption(document.getDocumentType());
        $(PROCEDURE_NUMBER_INPUT).setValue(document.getLinkedNotice().getNumber());
        $(SEARCH_BUTTON).click();
        $(RESULT_PROCEDURE_NUMBER).shouldHave(text(document.getLinkedNotice().getNumber()));
    }

    public static void openContextMenu() {
        $(RESULT_PROCEDURE_NUMBER).contextClick();
    }

    public static void makeProtocol() {
        $(RESULT_PROCEDURE_CHECKBOX).click();
        ActionsMenuPage.makeProtocol();
    }

}
