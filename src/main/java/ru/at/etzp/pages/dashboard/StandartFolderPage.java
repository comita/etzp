package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.models.documents.BaseDocument;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class StandartFolderPage extends BasePage {

    private static final By DOCUMENT_TYPE_LIST = id("filterDocumentType");
    private static final By SEARCH_INPUT = id("filterProcedureName");
    private static final By SEARCH_BUTTON = id("applyButton");
    private static final By CLEAR_BUTTON = id("clearButton");
    private static final By SELECT_ALL = id("toggleAll");

    private static final By RESULT_PROCEDURE_NUMBER = css("#doclistCenter tr:first-child+tr td:first-child+td+td+td>span:first-child");

    public static void searchForDocument(BaseDocument document) {
        $(CLEAR_BUTTON).click();
        $(DOCUMENT_TYPE_LIST).selectOption(document.getDocumentType());
        $(SEARCH_INPUT).setValue(document.getLinkedNotice().getNumber());
        $(SEARCH_BUTTON).click();
        $(RESULT_PROCEDURE_NUMBER).shouldHave(text(document.getLinkedNotice().getNumber()));
    }

    public static void selectAllDocuments() {
        $(SELECT_ALL).click();
    }
}
