package ru.at.etzp.pages.dashboard;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class ContextMenuPage extends BasePage {

    private static final By EDIT_MENU = css("[id*=DOCMENU] > [name=edit]");
    private static final By OPERATIONS_MENU = css("#workspaceCenter table + [id*=DOCMENU] > li + li + li + li + li + li + li + li + li + li");
    private static final By CHANGE_NOTICE_SUBMENU = css("[id*=DOCMENU] [name='changes']");

    public static void editDocument() {
        $(EDIT_MENU).click();
    }

    public static void changeNotice() {
        $(OPERATIONS_MENU).hover();
        $(CHANGE_NOTICE_SUBMENU).click();
    }

}
