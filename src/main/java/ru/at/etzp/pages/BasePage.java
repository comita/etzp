package ru.at.etzp.pages;

import org.openqa.selenium.By;

public abstract class BasePage {

    protected static By id(String id) {
        return new By.ById(id);
    }

    protected static By name(String name) {
        return new By.ByName(name);
    }

    protected static By css(String css) {
        return new By.ByCssSelector(css);
    }

    protected static By xpath(String xpath) {
        return new By.ByXPath(xpath);
    }
}
