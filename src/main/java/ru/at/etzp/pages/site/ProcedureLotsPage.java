package ru.at.etzp.pages.site;

import org.openqa.selenium.By;
import ru.at.etzp.tools.WindowsSwitcher;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class ProcedureLotsPage extends BasePage {

    private static final By LOT_OFFER_BUTTON = css("#listOfLots tr:first-child + tr #offerButton");

    public static void makeOfferForFirstLot() {
        WindowsSwitcher.prepareForWindowsSwitch();
        $(LOT_OFFER_BUTTON).click();
        WindowsSwitcher.switchToNewBrowserWindow();
    }
}
