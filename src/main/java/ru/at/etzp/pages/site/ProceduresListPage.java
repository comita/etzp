package ru.at.etzp.pages.site;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Condition.visible;

public class ProceduresListPage extends BasePage {

    private static final By PROCEDURE_NUMBER_INPUT = name("p_cnumber");
    private static final By SEARCH_BUTTON = css(".searchFormButton");
    private static final By RESULT_PROCEDURE_NUMBER = css(".tableProc tr:first-child + tr td:first-child + td");

    public static void checkPageLoadedCorrectly() {
        $(PROCEDURE_NUMBER_INPUT).shouldBe(visible);
    }

    public static void searchByNumber(String number) {
        $(PROCEDURE_NUMBER_INPUT).setValue(number);
        $(SEARCH_BUTTON).click();
        $(RESULT_PROCEDURE_NUMBER).shouldHave(text(number));
    }

    public static void openSearchResult() {
        $(RESULT_PROCEDURE_NUMBER).click();
    }

}
