package ru.at.etzp.pages.site;

import org.openqa.selenium.By;
import ru.at.etzp.tools.WindowsSwitcher;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;

public class ProcedureDocumentationPage extends BasePage {

    private static final By DESCRIPTION_TABLE = id("descriptionTable");
    private static final By REQUEST_BUTTON = id("orgRequest");
    private static final By LOTS_TAB = id("lots");

    public static void checkPageLoadedCorrectly() {
        $(DESCRIPTION_TABLE).shouldBe(visible);
    }

    public static void clickRequestButton() {
        WindowsSwitcher.prepareForWindowsSwitch();
        $(REQUEST_BUTTON).click();
        WindowsSwitcher.switchToNewBrowserWindow();
    }

    public static void goToLotsTab() {
        $(LOTS_TAB).click();
    }

}
