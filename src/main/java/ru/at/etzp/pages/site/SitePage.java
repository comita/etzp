package ru.at.etzp.pages.site;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;
import ru.at.etzp.utils.PropertyLoader;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class SitePage extends BasePage {

    private static final By SEARCH_BUTTON = css(".date_bar4>input");

    private static final By OPEN_COMPETITION = css(".ocClass");
    private static final By TWO_STAGE_COMPETITION = css(".ocTwoClass");
    private static final By OPEN_AUCTION = css(".oaClass");
    private static final By REAL_ESTATE_AUCTION = css(".oaCRIClass");
    private static final By REQUEST_QUOTATIONS = css(".reqClass");
    private static final By REQUEST_OFFERS = css(".roClass");
    private static final By PREQUALIFY = css(".pqsClass");
    private static final By LIMITED_COMPETITION = css(".ocQPClass");
    private static final By LIMITED_AUCTION = css(".oaQPClass");

    private static final By ARCHIVE = css(".arcClass");

    public static void openSitePage() {
        open(PropertyLoader.loadProperty(PropertyLoader.URL_SITE));
    }

    public static void goToSearch() {
        $(SEARCH_BUTTON).click();
    }

    public static void goToOpenCompetition() {
        $(OPEN_COMPETITION).click();
    }

    public static void goToTwoStageCompetition() {
        $(TWO_STAGE_COMPETITION).click();
    }

    public static void goToOpenAuction() {
        $(OPEN_AUCTION).click();
    }

    public static void goToRealEstateAuction() {
        $(REAL_ESTATE_AUCTION).click();
    }

    public static void goToRequestQuotations() {
        $(REQUEST_QUOTATIONS).click();
    }

    public static void goToRequestOffers() {
        $(REQUEST_OFFERS).click();
    }

    public static void goToPrequalify() {
        $(PREQUALIFY).click();
    }

    public static void goToLimitedCompetition() {
        $(LIMITED_COMPETITION).click();
    }

    public static void goToLimitedAuction() {
        $(LIMITED_AUCTION).click();
    }

    public static void goToArchive() {
        $(ARCHIVE).click();
    }

}
