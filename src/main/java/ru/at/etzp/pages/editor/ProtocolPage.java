package ru.at.etzp.pages.editor;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import ru.at.etzp.models.documents.RebiddingOffline;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.confirm;

public class ProtocolPage extends BasePage {

    private static final By OPEN_TABLE_BUTTON = css(".editButton");
    private static final By ADMIT_LIST = css("[name=Допуск]");
    private static final By ADD_ROW_BUTTON = css("[name=addRow]");
    private static final By STEPS_LIST = css("[name=Этап]");
    private static final By STEP_START_DATE = css("[name=ContestDate]");
    private static final By STEP_END_DATE = css("[name=DateEnd]");
    private static final By INVITE_BUTTON = css("[actionname=createInvite]");

    private static final String ALLOWED = "1";

    private static final String REBIDDING_OFFLINE_VALUE = "REBIDDING_OFFLINE";


    public static void admitAllParticipants() {
        openAllTables();
        admitParticipants();
    }

    private static void openAllTables() {
        ElementsCollection openTableButtons = $$(OPEN_TABLE_BUTTON);
        openTableButtons.get(0).click();
        openTableButtons.get(1).click();
    }

    private static void admitParticipants() {
        ElementsCollection admitLists = $$(ADMIT_LIST);
        admitLists.get(0).selectOptionByValue(ALLOWED);
        TopEditorPage.save();
    }

    public static void addRebiddingOffline(RebiddingOffline rebiddingOffline) {
        $(ADD_ROW_BUTTON).click();
        getLastStepList().selectOptionByValue(REBIDDING_OFFLINE_VALUE);
        setDate(getLastStartDate(), rebiddingOffline.getStartDate());
        setDate(getLastEndDate(), rebiddingOffline.getEndDate());
        TopEditorPage.save();
    }

    private static SelenideElement getLastStepList() {
        ElementsCollection stepLists = $$(STEPS_LIST);
        return stepLists.get(stepLists.size() - 1);
    }

    private static SelenideElement getLastStartDate() {
        ElementsCollection startDates = $$(STEP_START_DATE);
        return startDates.get(startDates.size() - 1);
    }

    private static SelenideElement getLastEndDate() {
        ElementsCollection endDates = $$(STEP_END_DATE);
        return endDates.get(endDates.size() - 1);
    }

    private static SelenideElement getLastInviteButton() {
        ElementsCollection inviteButtons = $$(INVITE_BUTTON);
        return inviteButtons.get(inviteButtons.size() - 1);
    }

    private static void setDate(SelenideElement dateInput, DateTime date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.YYYY HH:mm");
        dateInput.setValue(formatter.print(date));
    }

    public static void makeInvites() {
        getLastInviteButton().click();
        confirm(null);
    }

}
