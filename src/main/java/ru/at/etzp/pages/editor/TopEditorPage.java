package ru.at.etzp.pages.editor;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class TopEditorPage extends BasePage {
    private static final By SAVE = id("btnSave");
    private static final By SAVE_AND_SEND = id("btnSaveAndSend");
    private static final By SAVE_AND_CLOSE = id("btnSaveAndClose");
    private static final By CLOSE = id("btnClose");

    public static void save() {
        $(SAVE).click();
    }

    public static void saveAndSend() {
        $(SAVE_AND_SEND).click();
    }

    public static void saveAndClose() {
        $(SAVE_AND_CLOSE).click();
    }


    public static void close() {
        $(CLOSE).click();
    }
}
