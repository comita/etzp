package ru.at.etzp.pages.editor;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class ImportPage extends BasePage {

    private static final By FILE_INPUT = id("mainFileActionInput");
    private static final By OK_BUTTON = css("div[role=dialog] button");

    public static void uploadFile(String fullPathToFile) {
        $(FILE_INPUT).setValue(fullPathToFile);
        $(OK_BUTTON).click();
        $(OK_BUTTON).click();
    }

}
