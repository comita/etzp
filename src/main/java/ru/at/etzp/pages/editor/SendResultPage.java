package ru.at.etzp.pages.editor;

import org.openqa.selenium.By;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Condition.disappear;
import static com.codeborne.selenide.Selenide.$;

public class SendResultPage extends BasePage {

    private static final By PROGRESS_BAR = id("progressBar");
    private static final By CLOSE_BUTTON = css(".ui-icon.ui-icon-closethick");

    public static void checkResultIsCorrect() {
        $(PROGRESS_BAR).should(disappear);
        // TODO добавить проверку отправки
    }

    public static void close() {
        $(CLOSE_BUTTON).click();
    }
}
