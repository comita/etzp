package ru.at.etzp.pages.editor;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.openqa.selenium.By;
import ru.at.etzp.models.documents.notice.*;
import ru.at.etzp.pages.BasePage;

import static com.codeborne.selenide.Selenide.$;

public class NoticePage extends BasePage {
    private static final By IMPORT_BUTTON = id("importNomenclatureBtn");

    private static final By PROCEDURE_TYPE = id("ContestType");
    private static final By PROCEDURE_NUMBER = id("ContestNumber");
    private static final By PROCEDURE_NAME = id("ContestName");

    private static final By START_OFFER_DATE = id("ContestDateOADisplay");
    private static final By START_PROCEDURE_DATE = id("ContestDateDisplay");
    private static final By END_OFFER_DATE = id("DateEndDisplay");

    private static final By LOT_NAME = id("TableFieldID2");

    private static final By PRICE = id("SumDoc");
    private static final By FILL_PRICE_BUTTON = id("fillStartPriceBtn");

    private static final By DEAL_TYPE = id("TransactionType");
    private static final By OBJECT_NAME = id("ObjectName");
    private static final By OBJECT_ADDRESS = id("ObjectAddress");
    private static final By FILL_NAME_BUTTON = id("fillCRIBtn");
    private static final By STEP_PRICE = id("StepPrice");
    private static final By MINIMUM_PRICE = id("MINPRICELOT");

    public static void importNomenclatureFile(String filePath) {
        $(IMPORT_BUTTON).click();
        ImportPage.uploadFile(filePath);
    }

    private static void fillCommonFields(BaseNotice notice) {
        $(PROCEDURE_TYPE).selectOption(notice.getFullProcedureTypeName());
        $(PROCEDURE_NUMBER).val(notice.getNumber());
        $(PROCEDURE_NAME).val(notice.getName());
        setDate(START_PROCEDURE_DATE, notice.getStartDate());
        setEndOfferDate(notice);
    }

    public static void setEndOfferDate(BaseNotice notice) {
        setDate(END_OFFER_DATE, notice.getEndDate());
    }

    private static void setDate(By dateInput, DateTime date) {
        DateTimeFormatter formatter = DateTimeFormat.forPattern("dd.MM.YYYY HH:mm");
        $(dateInput).val(formatter.print(date));
        $(PROCEDURE_NUMBER).click();
    }

    public static void fillAs(OpenCompetitionNotice openCompetitionNotice) {
        fillCommonFields(openCompetitionNotice);

        $(LOT_NAME).val("Лот");
    }

    public static void fillAsOpenAuction(OpenAuctionNotice openAuctionNotice) {
        fillCommonFields(openAuctionNotice);

        setDate(START_OFFER_DATE, openAuctionNotice.getStartOfferDate());
        $(PRICE).val("" + openAuctionNotice.getPrice());
        $(FILL_PRICE_BUTTON).click();
    }

    public static void fillAsClosedAuction(ClosedAuctionNotice closedAuctionNotice) {
        fillAsOpenAuction(closedAuctionNotice);
    }

    public static void fillAsRealEstateAuction(RealEstateAuctionNotice realEstateAuctionNotice) {
        fillAsOpenAuction(realEstateAuctionNotice);

        $(DEAL_TYPE).selectOption(realEstateAuctionNotice.getDealType());
        $(OBJECT_NAME).val(realEstateAuctionNotice.getObjectName());
        $(OBJECT_ADDRESS).val(realEstateAuctionNotice.getObjectAddress());
        $(FILL_NAME_BUTTON).click();
        $(STEP_PRICE).val("" + realEstateAuctionNotice.getStepPrice());
    }

    public static void fillAsRealEstateDownAuction(RealEstateDownAuctionNotice realEstateDownAuctionNotice) {
        fillAsRealEstateAuction(realEstateDownAuctionNotice);

        $(MINIMUM_PRICE).val("" + realEstateDownAuctionNotice.getMinimumPrice());
    }
}
