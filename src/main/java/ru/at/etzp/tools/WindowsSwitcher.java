package ru.at.etzp.tools;

import com.codeborne.selenide.Selenide;
import com.google.common.base.Predicate;
import org.openqa.selenium.NoSuchWindowException;
import org.openqa.selenium.WebDriver;

import java.util.Set;

import static com.codeborne.selenide.Selenide.switchTo;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class WindowsSwitcher {

    private static Set<String> oldWindowsHandles;

    public static void prepareForWindowsSwitch() {
        oldWindowsHandles = getWebDriver().getWindowHandles();
    }

    public static void switchToNewBrowserWindow() throws NoSuchWindowException {
        try {
            Selenide.Wait().until(new Predicate<WebDriver>() {
                @Override
                public boolean apply(WebDriver webDriver) {
                    return !getWebDriver().getWindowHandles().equals(oldWindowsHandles);
                }
            });
        } catch (Exception exception) {
            throw new NoSuchWindowException("Новое окно не появилось за время отведенное WebDriverWait!");
        }

        for (String windowHandle : getWebDriver().getWindowHandles()) {
            if (oldWindowsHandles.contains(windowHandle)) {
                switchTo().window(windowHandle);
                getWebDriver().close();
            }
        }

        for (String windowHandle : getWebDriver().getWindowHandles()) {
            if (!oldWindowsHandles.contains(windowHandle)) {
                switchTo().window(windowHandle);
            }
        }

        getWebDriver().manage().window().maximize();
    }

}