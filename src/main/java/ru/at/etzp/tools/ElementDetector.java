package ru.at.etzp.tools;

import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.appear;
import static com.codeborne.selenide.Selenide.$;

public class ElementDetector {

    public static String idThatPresentsOnPage(String firstId, String secondId) {
        return $(By.cssSelector(String.format("#%s, #%s", firstId, secondId))).shouldBe(appear).getAttribute("id");
    }

}