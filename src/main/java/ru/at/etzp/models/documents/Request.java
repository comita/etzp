package ru.at.etzp.models.documents;

import ru.at.etzp.models.documents.notice.BaseNotice;

public class Request extends BaseDocument {

    public Request(BaseNotice notice) {
        super(notice);
    }

    @Override
    public String getDocumentType() {
        return "Запрос заказчику";
    }

}