package ru.at.etzp.models.documents.notice;

public class RealEstateDownAuctionNotice extends RealEstateAuctionNotice {
    private float minimumPrice;

    public RealEstateDownAuctionNotice() {
        super();

        minimumPrice = 2000000;
    }

    @Override
    public String getFullProcedureTypeName() {
        return "Открытый аукцион по недвижимости с возможным понижением";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "АПНпониж";
    }

    public float getMinimumPrice() {
        return minimumPrice;
    }
}
