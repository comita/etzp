package ru.at.etzp.models.documents.notice;

public class OpenCompetitionNotice extends BaseNotice {

    @Override
    public String getFullProcedureTypeName() {
        return "Открытый конкурс";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "ОК";
    }

}