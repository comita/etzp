package ru.at.etzp.models.documents.notice;

public class ClosedCompetitionRZDSNotice extends BaseNotice {

    @Override
    public String getFullProcedureTypeName() {
        return "Конкурс среди организаций, прошедших квалификационный отбор РЖДС";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "КО_РЖДС";
    }
}
