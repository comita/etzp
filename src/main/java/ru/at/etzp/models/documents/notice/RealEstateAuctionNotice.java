package ru.at.etzp.models.documents.notice;

public class RealEstateAuctionNotice extends OpenAuctionNotice {
    private String dealType;
    private String objectName;
    private String objectAddress;
    private float stepPrice;

    public RealEstateAuctionNotice() {
        super();

        dealType = "Продажа";
        objectName = "Столовая";
        objectAddress = "Санкт-Петербург";
        stepPrice = 10000;
    }

    @Override
    public String getFullProcedureTypeName() {
        return "Открытый аукцион по недвижимости";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "АПН";
    }

    public String getDealType() {
        return dealType;
    }

    public String getObjectName() {
        return objectName;
    }

    public String getObjectAddress() {
        return objectAddress;
    }

    public float getStepPrice() {
        return stepPrice;
    }
}
