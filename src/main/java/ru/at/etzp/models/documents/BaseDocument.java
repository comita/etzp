package ru.at.etzp.models.documents;

import ru.at.etzp.models.documents.notice.BaseNotice;

public abstract class BaseDocument {

    private BaseNotice notice;

    public BaseDocument(BaseNotice notice) {
        this.notice = notice;
    }

    public BaseNotice getLinkedNotice() {
        return notice;
    }

    public abstract String getDocumentType();

}