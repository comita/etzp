package ru.at.etzp.models.documents.notice;

public class ClosedAuctionNotice extends OpenAuctionNotice {
    @Override
    public String getFullProcedureTypeName() {
        return "Аукцион с ограниченным участием";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "АОУ";
    }
}
