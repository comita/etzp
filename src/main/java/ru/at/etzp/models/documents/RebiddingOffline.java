package ru.at.etzp.models.documents;

import org.joda.time.DateTime;
import ru.at.etzp.models.documents.notice.BaseNotice;

public class RebiddingOffline extends BaseDocument {
    protected DateTime startDate;
    protected DateTime endDate;

    public RebiddingOffline(BaseNotice notice) {
        super(notice);
        startDate = new DateTime().minusDays(1);
        endDate = new DateTime().plusHours(1);
    }

    @Override
    public String getDocumentType() {
        return "Уведомление об участии";
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }
}
