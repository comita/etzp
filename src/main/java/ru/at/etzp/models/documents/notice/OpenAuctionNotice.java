package ru.at.etzp.models.documents.notice;

import org.joda.time.DateTime;

public class OpenAuctionNotice extends BaseNotice {
    private DateTime startOfferDate;
    private double price;

    public OpenAuctionNotice() {
        super();

        startOfferDate = new DateTime().minusDays(1);
        endDate = new DateTime().plusMonths(1);
        startDate = endDate.plusMonths(1).plusMinutes(30);

        price = 2500000;
    }

    @Override
    public String getFullProcedureTypeName() {
        return "Открытый аукцион";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "ОА";
    }

    public DateTime getStartOfferDate() {
        return startOfferDate;
    }

    public double getPrice() {
        return price;
    }
}
