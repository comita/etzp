package ru.at.etzp.models.documents.notice;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import ru.at.etzp.models.documents.BaseDocument;

public abstract class BaseNotice extends BaseDocument {
    private String number;
    private String name;

    protected DateTime startDate;
    protected DateTime endDate;

    public BaseNotice() {
        super(null);

        generateNameAndNumber();

        startDate = new DateTime().minusDays(1);
        endDate = new DateTime().plusHours(1);
    }

    @Override
    public String getDocumentType() {
        return "Извещение о процедуре";
    }

    @Override
    public BaseNotice getLinkedNotice() {
        return this;
    }

    private void generateNameAndNumber() {
        DateTime dateTime = new DateTime();
        DateTimeFormatter formatter = DateTimeFormat.forPattern("ddMMYYYY");

        setNumber("УКК(2.2.8.2)-" + formatter.print(dateTime) + "-" + getShortProcedureTypeName() + "-" + Long.toString(dateTime.getMillis()) + "-номер");
        setName("УКК(2.2.8.2)-" + formatter.print(dateTime) + "-" + getShortProcedureTypeName() + "-" + Long.toString(dateTime.getMillis()) + "-наименование");
    }

    public abstract String getFullProcedureTypeName();

    public abstract String getShortProcedureTypeName();

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public DateTime getStartDate() {
        return startDate;
    }

    public DateTime getEndDate() {
        return endDate;
    }

    public void setEndDateToCurrent() {
        endDate = new DateTime();
    }

}