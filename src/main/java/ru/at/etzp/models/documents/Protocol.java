package ru.at.etzp.models.documents;

import ru.at.etzp.models.documents.notice.BaseNotice;

public class Protocol extends BaseDocument {

    public Protocol(BaseNotice notice) {
        super(notice);
    }

    @Override
    public String getDocumentType() {
        return "Протокол";
    }

}