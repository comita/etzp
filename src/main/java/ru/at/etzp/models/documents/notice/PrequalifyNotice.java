package ru.at.etzp.models.documents.notice;

public class PrequalifyNotice extends BaseNotice {

    @Override
    public String getFullProcedureTypeName() {
        return "Предварительный квалификационный отбор";
    }

    @Override
    public String getShortProcedureTypeName() {
        return "ПКО";
    }

}
