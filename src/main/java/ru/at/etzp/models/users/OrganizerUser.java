package ru.at.etzp.models.users;

import ru.at.etzp.database.DBBase;
import ru.at.etzp.database.DBOrganizer;

public class OrganizerUser extends BaseUser {

    public OrganizerUser() {
        super(Role.Organizer);

        setLogin("orguser18");
        setPassword("00001");
    }

    @Override
    protected DBBase getDBConnection() {
        return new DBOrganizer();
    }

}