package ru.at.etzp.models.users;

import ru.at.etzp.database.DBBase;
import ru.at.etzp.database.DBParticipant;

public class ParticipantUser extends BaseUser {

    public ParticipantUser() {
        super(Role.Participant);

        setLogin("uch_saharov_1");
        setPassword("gfhgfhgfh");
    }

    @Override
    protected DBBase getDBConnection() {
        return new DBParticipant();
    }

}