package ru.at.etzp.models.users;

import ru.at.etzp.database.DBBase;
import ru.at.etzp.utils.Log;

import java.sql.SQLException;

public abstract class BaseUser {

    private Role role;
    private String login;
    private String password;

    public enum Role {Organizer, Participant};

    public BaseUser(Role role) {
        this.role = role;

        try {
            getDBConnection().clearSortProperties(this);
        } catch (SQLException e) {
            Log.write("Ошибка при очистке сортировки для пользователя " + getLogin() + "!");
        }
    }

    public Role getRole() {
        return role;
    }

    protected void setLogin(String login) {
        this.login = login;
    }

    public String getLogin() {
        return login;
    }

    protected void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public BaseUser setEmptyLogin() {
        login = "";

        return this;
    }

    public BaseUser setEmptyPassword() {
        password = "";

        return this;
    }

    public BaseUser setIncorrectLogin() {
        login = "123";

        return this;
    }

    public BaseUser setIncorrectPassword() {
        password = "123";

        return this;
    }

    protected abstract DBBase getDBConnection();

}