package ru.at.etzp.database;

public class DBParticipant extends DBBase {

    private static final String DB_PARTICIPANT_URL = "jdbc:oracle:thin:@VM-ETZP3-ORA:1521:orcl";
    private static final String DB_PARTICIPANT_USERNAME = "ccwe";
    private static final String DB_PARTICIPANT_PASSWORD = "ccwe";

    public DBParticipant() {
        super(DB_PARTICIPANT_URL, DB_PARTICIPANT_USERNAME, DB_PARTICIPANT_PASSWORD);
    }

}