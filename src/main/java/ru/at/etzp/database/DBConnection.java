package ru.at.etzp.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import ru.at.etzp.utils.Log;

public abstract class DBConnection {

    private Connection connection;

    public DBConnection(String url, String username, String password) {
        try {
            DriverManager.registerDriver(new oracle.jdbc.OracleDriver());
        } catch (SQLException exception) {
            Log.write("Не найден драйвер базы данных JDBC (ojdbc6.jar)!");
            exception.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException exception) {
            Log.write("Не удалось подключиться к базе данных: " + url + ", " + username + ", " + password);
        }
    }

    protected Connection getConnection() {
        return connection;
    }

    protected void closeConnection() {
        try {
            connection.close();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

}