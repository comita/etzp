package ru.at.etzp.database;

import ru.at.etzp.models.users.BaseUser;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public abstract class DBBase extends DBConnection {

    public DBBase(String url, String username, String password) {
        super(url, username, password);
    }

    public void clearSortProperties(BaseUser user) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement("delete from user_props where name like ? and user_id in ( select id from users where login_name = ? )");

        statement.setString(1, "sort%");
        statement.setString(2, user.getLogin());

        statement.executeQuery();

        getConnection().commit();

        closeConnection();
    }

}