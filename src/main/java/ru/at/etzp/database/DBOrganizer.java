package ru.at.etzp.database;

public class DBOrganizer extends DBBase {

    private static final String DB_ORGANIZER_URL = "jdbc:oracle:thin:@VM-ETZP3-ORA:1521:orcl";
    private static final String DB_ORGANIZER_USERNAME = "ccweorg";
    private static final String DB_ORGANIZER_PASSWORD = "ccweorg";

    public DBOrganizer() {
        super(DB_ORGANIZER_URL, DB_ORGANIZER_USERNAME, DB_ORGANIZER_PASSWORD);
    }

}